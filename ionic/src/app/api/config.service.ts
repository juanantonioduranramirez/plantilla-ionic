import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  baseUrlApi = "http://localhost/scaffolding-codeigniter-api/"

  constructor() { }

  getBaseUrlApi()
  {
    return this.baseUrlApi;
  }
}
