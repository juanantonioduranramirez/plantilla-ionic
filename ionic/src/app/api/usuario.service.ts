import { Injectable } from '@angular/core';

import { ConfigService } from './config.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {


  usuario

  constructor(
    private http: HttpClient, 
    private config: ConfigService) {}

  get(email, password)
  {

    var form = new FormData();

    form.append("email", email);
    form.append("password",password);

    return this.http.post(this.config.getBaseUrlApi()+'usuario', form);

  }

  guardarUsuarioLogin(usuario)
  {
    this.usuario = usuario;
  }

  registro(usuario)
  {

    var form = new FormData();

    form.append("email", usuario.email);
    form.append("password", usuario.password1);
    form.append("nombre", usuario.nombre); 
    
    return this.http.post(this.config.getBaseUrlApi()+'registro', form);

  }

  getUsuarioLogin()
  {
    return this.usuario;
  }

}
