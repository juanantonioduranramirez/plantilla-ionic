import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api/paciente.service';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-pacientes',
  templateUrl: './pacientes.page.html',
  styleUrls: ['./pacientes.page.scss'],
})
export class PacientesPage implements OnInit {

  pacientes = []
  pagina = 0;

  connectionError = false

  constructor(
    public ser: ApiService,
    public loadingController: LoadingController,
    public router: Router,
    public toastController: ToastController) 
  {
    this.get();
  }

  ngOnInit() {}

  async get()
  {

    this.connectionError = false

    const loading = await this.loadingController.create(
    {
      message: 'Cargando...'
    });

    await loading.present();

    this.ser.get(this.pagina).subscribe((data)=>
    {
      console.log(data);

      for (let i = 0; i < data['data'].length; i++) 
      {
        this.pacientes.push(data['data'][i])
      }

      loading.dismiss();

    },(err:HttpErrorResponse)=>
    { 
      this.connectionError = true;
      loading.dismiss();

    }),

    (error)=> 
    {
      loading.dismiss();
      console.log(error);
    }
      
  }

  doRefresh(event)
  {

    this.pagina = 0;
    this.pacientes = [];
    
    this.ser.get(this.pagina).subscribe((data)=>
    {

      data['data'].forEach(element => 
      {
        this.pacientes.push(element)
      });

      setTimeout(() => 
      {
        event.target.complete();
      });


    }, (err:HttpErrorResponse)=>
    { 
      this.connectionError = true;
      event.target.complete();
    }),

    (error)=> 
    {
      
      event.target.complete();

      console.log(error);
    }

  }

  // async presentToast(mensaje) 
  // {
  //   const toast = await this.toastController.create({
  //     message: mensaje,
  //     duration: 2000
  //   });
  //   toast.present();
  // }

  loadData(event) 
  {

    this.connectionError = false

    this.pagina = this.pagina +1;    

    this.ser.get(this.pagina).subscribe((data)=>
    {

      if(data['data'].length == 0)
      {
        setTimeout(() => 
        {   
          event.target.complete();    
        });
      }

      data['data'].forEach(element => 
      {
        this.pacientes.push(element)

        setTimeout(() => 
        {
          event.target.complete();
        });

      });

    },(err:HttpErrorResponse)=>
    { 
      this.connectionError = true;
      event.target.complete();
    }),

    (error)=> 
    {
      event.target.complete();

      console.log(error);
    }
  }

  abrirPaciente(id)
  {

    console.log("id del paciente");
    console.log(id);
    

    console.log("abriendo paciente");
    this.router.navigateByUrl('paciente-detail/'+id)
  }

}
