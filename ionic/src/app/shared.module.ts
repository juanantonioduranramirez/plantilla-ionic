import { NgModule }       from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';  
import { HeaderComponent } from './components/header/header.component';

@NgModule({
    imports: [
      IonicModule,
      RouterModule,
      CommonModule
    ],
    declarations: [
      HeaderComponent

    ],
    providers: [],
    exports: [
      HeaderComponent
    ]
})
export class SharedModule {}