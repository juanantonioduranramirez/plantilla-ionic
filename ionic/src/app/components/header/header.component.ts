import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/api/usuario.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  usuario

  constructor(public ser: UsuarioService) 
  {
    
    this.usuario = this.ser.getUsuarioLogin();
    
  }

  ngOnInit() 
  {
  }

}
