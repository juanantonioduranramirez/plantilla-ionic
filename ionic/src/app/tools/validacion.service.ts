import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidacionService {

  erroresValidacion = []

  constructor() {}

  hayCamposVacios(usuario, accion)
  {
    
    let valor = 'ok'
    Object.keys(usuario).forEach(function(i) 
    {
      if(usuario[i] == "")
      valor = 'error'
    });

    if(valor =='error' && accion == 'imprimirMensaje')
    this.erroresValidacion.push('<br /> todos los campos son obligatorios')

    return valor
  }

  compararClaves(formRegistro, accion)
  {

    let valor

    if(formRegistro.password1 == formRegistro.password2)
    return valor = 'ok'
    else
    {
      if(accion == 'imprimirMensaje')
      this.erroresValidacion.push('<br /> error claves no coinciden')

      return valor = 'error'
    }
  }

  validarEmail(email, accion)
  {
    let valor

    if(email.includes("@") && email.includes("."))
    return valor = 'ok'
    else
    {
      if(accion == 'imprimirMensaje')
      this.erroresValidacion.push('<br /> error formato email')      
      
      return valor = 'error' 
    }
  }

  validarLongitud(campo, length, accion)
  {

    let valor

    if(campo.length >= length)
    return valor = 'ok'
    else
    {

      if(accion == 'imprimirMensaje')
      this.erroresValidacion.push('<br /> '+campo+" debe tener al menos "+length+" caracteres")

      return valor = 'error'
    }
  }

  getErroresValidacion()
  {
    return this.erroresValidacion;
  }

  resetErroresValidacion()
  {
    this.erroresValidacion = [];
  }


}
