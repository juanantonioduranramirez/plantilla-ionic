import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PacienteDetailPage } from './paciente-detail.page';
import { SharedModule } from '../shared.module';

const routes: Routes = [
  {
    path: '',
    component: PacienteDetailPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PacienteDetailPage]
})
export class PacienteDetailPageModule {}
