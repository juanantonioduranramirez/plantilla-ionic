import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api/paciente.service';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-paciente-detail',
  templateUrl: './paciente-detail.page.html',
  styleUrls: ['./paciente-detail.page.scss'],
})
export class PacienteDetailPage implements OnInit {

  paciente = {}

  constructor(public ser: ApiService, 
              public route: ActivatedRoute,
              public alertController: AlertController) {}

  ngOnInit() 
  {
    this.route.params.subscribe(params => 
    {
      this.get(params['id'])
    });
  }

  get(id)
  {
    this.ser.getItem(id).subscribe((data)=>
    {
      this.paciente = data[0];
      
    }),

    (error)=> 
    {
      console.log(error);
    }
  }

  async eliminar() 
  {
    const alert = await this.alertController.create({
      header: 'Alerta',
      subHeader: 'Eliminando paciente...',
      message: '¿Está seguro de eliminar el paciente?.',

      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => 
          {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Sí',
          handler: () => 
          {
            console.log('Borrando ... ');
          }
        }
      ]

    });

    await alert.present();
  }

}
