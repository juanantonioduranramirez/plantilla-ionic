import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../api/usuario.service';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ValidacionService } from '../tools/validacion.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  tipoCampoPassword1= 'password'
  tipoCampoPassword2= 'password'

  nombre : String = ""
  email : String = ""
  password1 : String = ""
  password2 : String = ""

  constructor(public ser: UsuarioService,
              public alertController: AlertController,
              public router: Router,
              public validacion : ValidacionService
              ) { }

  ngOnInit() {
  }

  registro()
  {

    let usuario = 
    {
      nombre: this.nombre,
      email: this.email,
      password1: this.password1,
      password2: this.password2
    }

    if(this.validacion.hayCamposVacios(usuario, 'validar') == 'ok' &&
      this.validacion.compararClaves(usuario, 'validar') == 'ok' &&
      this.validacion.validarEmail(usuario.email, 'validar') == 'ok' &&
      this.validacion.validarLongitud(usuario.nombre, 6, 'validar') == 'ok' &&
      this.validacion.validarLongitud(usuario.password1, 6, 'validar') == 'ok')
    {
      this.ser.registro(usuario).subscribe((data)=>
      { 
        this.router.navigateByUrl('home')            
      }),
  
      (error)=> 
      {
        console.log(error);
      }
    }
    else
    {
      this.validacion.hayCamposVacios(usuario, 'imprimirMensaje')
      this.validacion.compararClaves(usuario, 'imprimirMensaje')
      this.validacion.validarEmail(usuario.email, 'imprimirMensaje')
      this.validacion.validarLongitud(usuario.nombre, 6, 'imprimirMensaje')
      this.validacion.validarLongitud(usuario.password1, 6, 'imprimirMensaje')

      let alert = ''

      console.log(this.validacion.getErroresValidacion());
      
      this.validacion.getErroresValidacion().forEach(mensajeValidacion => 
      {        
        alert+= mensajeValidacion
      });
      
      this.presentAlert(alert);
      this.validacion.resetErroresValidacion();
    }
    
  }

  async presentAlert(mensaje) {
    const alert = await this.alertController.create({
      header: 'Info',
      message: mensaje,
      buttons: ['Ok']
    });

    await alert.present();
  }

  verClave1()
  {
    
    if(this.tipoCampoPassword1 == 'text')
    this.tipoCampoPassword1 = 'password';
    else
    this.tipoCampoPassword1 = 'text';

  }

  verClave2()
  {
    
    if(this.tipoCampoPassword2 == 'text')
    this.tipoCampoPassword2 = 'password';
    else
    this.tipoCampoPassword2 = 'text';

  }

}
