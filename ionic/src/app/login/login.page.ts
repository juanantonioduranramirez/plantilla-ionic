import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { UsuarioService } from '../api/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email
  password
  
  tipoCampoPassword = 'password';

  constructor(
              public ser: UsuarioService,
              public router: Router,
              public alertController: AlertController) { }

  ngOnInit() 
  {
    
  }

  acceder()
  {
    this.ser.get(this.email, this.password)  .subscribe(res => 
    {
      if(res == 'error')
      this.presentAlert();
      else
      this.router.navigateByUrl('home')

      //guardamos los datos del usuario en el service
      this.ser.guardarUsuarioLogin(res[0]);
      
    }, 
    (err) => 
    {
      console.log(err);
    });

  }

  registro()
  {
    this.router.navigateByUrl('signup')
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Alerta',
      subHeader: 'Datos incorrectos',
      message: 'Error de conexión.',
      buttons: ['Volver a intentar']
    });

    await alert.present();
  }

  verClave()
  {
    
    if(this.tipoCampoPassword == 'text')
    this.tipoCampoPassword = 'password';
    else
    this.tipoCampoPassword = 'text';

  }


}
